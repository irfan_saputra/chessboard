const makeChessboard = () => {
  let chessboard = [];
  for(var y = 0; y < 8; y++){
        var row = [];
        if(y==1){
          for(var x = 0; x < 8; x++){
            var cell = [];
            cell[x]='"P Black"';
            row.push(cell[x]);
          }
        }
        else if(y>=2 && y<=5){
          for(var x = 0; x < 8; x++){
            var cell = [];
            cell[x]='"-"';
            row.push(cell[x]);
          }
        }
       else if(y==6){
          for(var x = 0; x < 8; x++){
            var cell = [];
            cell[x]='"P White"';
            row.push(cell[x]);
          }
       }  
       else if(y==7){
          var cell = [];
          for(var x = 0; x < 8; x++){
            if(x==0 || x==7){
              cell[x]= '"B White"'
            }else if(x==1 || x==6){
              cell[x]='"K White"';    
            }else if(x==2 || x==5){
              cell[x]='"M White"';    
            }else if(x==3){
              cell[x]='"RT White"';    
            }else if(x==4){
              cell[x]='"RJ White"';    
            }
            row.push(cell[x]);
          }
       }  
       else if(y==0){
          var cell = [];
          for(var x = 0; x < 8; x++){
            if(x==0 || x==7){
              cell[x]= '"B Black"'
            }else if(x==1 || x==6){
              cell[x]='"K Black"';    
            }else if(x==2 || x==5){
              cell[x]='"M Black"';    
            }else if(x==3){
              cell[x]='"RT Black"';    
            }else if(x==4){
              cell[x]='"RJ Black"';    
            }
            row.push(cell[x]);
          }
       }  
      chessboard.push(row);
    }
  return chessboard;
}

const printBoard = x => {
for (var i=0;i<x.length;i++){
  console.log(x[i]);
 }
}

printBoard(makeChessboard());